import time
def tenure(principal, interest_rate, emi, round_currency = True):

    
    interest = principal * interest_rate/12
    amount = principal
    
    principal_repaid = 0
    interest_repaid = 0

    tenure = 1
    while amount >=  emi:
        
        interest = amount * interest_rate/12
        
        if round_currency:
            interest = float('{:.2}'.format(float(interest)))

        rem = emi - interest
    
        interest_repaid += interest
        principal_repaid += rem
        
        amount = amount - rem
        tenure = tenure + 1

        
        print('-----------------------------------------------------')
        print('log: interest_repaid this month  : {}'.format(interest))
        print('log: principal_repaid this month : {}'.format(emi - interest))
        print('log: interest_repaid             : {}'.format(interest_repaid))
        print('log: principal_repaid            : {}'.format(principal_repaid))
        print('log: amount                      : {}'.format(amount))
        print('log: interest                    : {}'.format(interest))
        
    interest = amount * interest_rate/12
    interest_repaid += interest
    
    principal_repaid += amount
    
    return principal_repaid, interest_repaid, tenure



if __name__ == '__main__':

    f = open('loan.csv', 'w')
    
    emi = [5000 + 1000 * i for i in range(11)]

    f.write('{}'.format(
        ','.join(
            ['rate of interest', 'principal', 'interest', 'total', 'tenure', 'emi']) + '\n')
    )

    for principal in [100000, 150000, 200000, 250000]:
        """
    for r in reversed(range(20)):
        rate = (12 + r * 0.5)/100
        """
        rate = 0.145
        row = []
        for e in reversed(emi):
            p, i, t = tenure(principal, rate, e)

            record = [rate, p, i, p + i, t, e]
            row.append(','.join([str(r) for r in record]))

            print('----------------------------------')
            print('repayments: ==')
            print(' principal:{}'.format(p))
            print('  interest:{}'.format(i))
            print('     total:{}'.format(p+i))
            print('    tenure:{}'.format(t))
            print('       emi:{}'.format(e))
            print('      rate:{}'.format(rate))
            print('----------------------------------')

        f.write('\n'.join(row + [' ', ' ', ' ', ' ', ' '] ))
 
